# File Upload Flow

## Overview

An update to the Lightning Quiz component adding the following features:

* Quiz is now timed and will auto submit/grade on timeout
* Users can no longer copy text or open the context menu of questions during the exam
* Questions now have topics associated with them
* The quiz results will show the user's score by topic (and their total score)
* Can now import quizes via single file into org

## Demo (~70 sec)

[Here's a link to a demo video for the flow](https://www.youtube.com/watch?v=ZHKJgs5S6Kw)

## Some Screenshots

### Quiz Timer

![Quiz Timer](media/timer.png)

### Quiz Result

![Quiz Result](media/result.png)

### Upload a Quiz

![Upload a Quiz](media/uploader.png)

### Examaple Quiz File

![Examaple Quiz File](media/import_file.png)