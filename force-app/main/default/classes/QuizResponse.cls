public with sharing class QuizResponse {
    @AuraEnabled
    public String tester;
    @AuraEnabled
    public List<QuestionWrapper> questions = new List<QuestionWrapper>();
    @AuraEnabled
    public Integer timeLimit;

    public QuizResponse(List<Quiz_Question__c> passedQuestions)
    {
        Integer count = 1;
        for(Quiz_Question__c eachQuestion : passedQuestions) {
            questions.add(new QuestionWrapper(eachQuestion, count));
            count++;
        }
    }

    public class QuestionWrapper {
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public String label;
        @AuraEnabled
        public Integer questionNumber;
        @AuraEnabled
        public List<AnswerWrapper> answerOptions = new List<AnswerWrapper>();
        @AuraEnabled
        public String topic;

        public QuestionWrapper(Quiz_Question__c question, Integer count)
        {
            System.debug('Question: ');
            System.debug(question.Body__c);
            System.debug(question.Answer_Option_A__r.Body__c + ' ' + question.Answer_Option_A__r.Id);
            System.debug(question.Answer_Option_B__r.Body__c + ' ' + question.Answer_Option_A__r.Id);
            System.debug(question.Answer_Option_C__r.Body__c + ' ' + question.Answer_Option_A__r.Id);
            System.debug(question.Answer_Option_D__r.Body__c + ' ' + question.Answer_Option_A__r.Id);
            System.debug(question.Answer_Option_E__r.Body__c + ' ' + question.Answer_Option_A__r.Id);
            id = question.Id;
            label = question.Body__c;
            questionNumber = count;
            topic = question.Topic__c;
            addNewAnswerWrapper(question.Answer_Option_A__r.Id, question.Answer_Option_A__r.Body__c);
            addNewAnswerWrapper(question.Answer_Option_B__r.Id, question.Answer_Option_B__r.Body__c);
            addNewAnswerWrapper(question.Answer_Option_C__r.Id, question.Answer_Option_C__r.Body__c);
            addNewAnswerWrapper(question.Answer_Option_D__r.Id, question.Answer_Option_D__r.Body__c);
            addNewAnswerWrapper(question.Answer_Option_E__r.Id, question.Answer_Option_E__r.Body__c);
        }

        private void addNewAnswerWrapper(Id answerOptionId, String answerOptionBody)
        {
            if(answerOptionId != null && !String.isEmpty(answerOptionBody)) {
                answerOptions.add(new AnswerWrapper(answerOptionId, answerOptionBody));
            }
        }
    }

    public class AnswerWrapper {
        @AuraEnabled
        public Id id;
        @AuraEnabled
        public String label;
        
        public AnswerWrapper(Id ansId, String ansBody)
        {
            id = ansId;
            label = ansBody;
        }
    }
}
