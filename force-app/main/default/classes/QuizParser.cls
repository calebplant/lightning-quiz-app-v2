public with sharing class QuizParser {

    private static final Map<String, Integer> colIndexByColName = new Map<String, Integer> {
        'Body' => 0,
        'Topic' => 1,
        'A' => 2,
        'B' => 3,
        'C' => 4,
        'D' => 5,
        'E' => 6,
        'Correct' => 7
    };

    @AuraEnabled
    public static void importQuiz(ParseRequest req)
    {
        System.debug('START importQuiz');
        System.debug(req);
        
        List<Id> importedRecords = new List<Id>();
        if(req.documentId != null) {
            ContentVersion objVersion = [SELECT Id, VersionData FROM ContentVersion WHERE ContentDocumentId =: req.documentId];
            List<String> fileLines = objVersion.VersionData.toString().split('\n');

            List<ParsedQuestion> parsedQuestions = parseQuestions(fileLines);

            // Check for previous questions
            Id questionSetId;
            List<Quiz_Question_Set__c> previousSet = [SELECT Id, Name FROM Quiz_Question_Set__c WHERE Name = :req.quizSetName];
            if(previousSet.size() > 0) {
                questionSetId = previousSet[0].Id;
                List<Quiz_Question__c> previousQuestionsInSet = getPreviousQuestions(questionSetId);
                delete previousQuestionsInSet;
            } else {
                Quiz_Question_Set__c newSet = new Quiz_Question_Set__c(Name = req.quizSetName, Time_Limit__c = req.timeLimit);
                insert newSet;
                questionSetId = newSet.Id;
                importedRecords.add(newSet.Id); // debug
            }
            
            // Create Answers
            Set<String> answerNames = compilePassedAnswerNames(parsedQuestions);
            Map<String, Id> existingAnswerByName = getExistingAnswers(answerNames);
            Map<String, Id> answerIdByName = createNewAnswers(parsedQuestions, existingAnswerByName);


            // Create questions
            Set<Id> insertedQuestions = createNewQuestions(parsedQuestions, answerIdByName);

            // Create Questions in Set
            createNewQuestionsInSet(insertedQuestions, questionSetId);
            Database.delete(req.documentId);

            // Debug
            // importedRecords.add(objVersion.Id);
            importedRecords.addall(answerIdByName.values());
            importedRecords.addall(insertedQuestions);

            Tester_Object__c debugObj = new Tester_Object__c(Quiz_Name__c = req.quizSetName, Record_List__c = String.join(importedRecords, ','));
            insert debugObj;
        }
    }

    private static List<ParsedQuestion> parseQuestions(List<String> fileLines) {
        System.debug('START parseQuestions');
        List<ParsedQuestion> result = new List<ParsedQuestion>();

        for(Integer n=1; n < fileLines.size(); n++){
            List<String> lineData = fileLines[n].split(',');
            // System.debug(lineData);

            ParsedQuestion newQuestion = new ParsedQuestion();
            newQuestion.body = lineData[colIndexByColName.get('Body')];
            newQuestion.topic = lineData[colIndexByColName.get('Topic')];
            newQuestion.answerA = lineData[colIndexByColName.get('A')];
            newQuestion.answerB = lineData[colIndexByColName.get('B')];
            newQuestion.answerC = lineData[colIndexByColName.get('C')];
            newQuestion.answerD = lineData[colIndexByColName.get('D')];
            newQuestion.answerE = lineData[colIndexByColName.get('E')];
            newQuestion.correctAnswer = lineData[colIndexByColName.get('Correct')];
            result.add(newQuestion);
        }

        for(ParsedQuestion eachQuestion : result) {
            System.debug(eachQuestion);
        }

        return result;
    }

    private static List<Quiz_Question__c> getPreviousQuestions(Id questionSetId) {
        return [SELECT Id, Body__c, Correct_Answer__r.Id, Correct_Answer__r.Name, Topic__c,
                    Answer_Option_A__r.Body__c, Answer_Option_B__r.Body__c,
                    Answer_Option_C__r.Body__c, Answer_Option_D__r.Body__c,
                    Answer_Option_E__r.Body__c
            FROM Quiz_Question__c
            WHERE Id IN (SELECT Quiz_Question__c
                        FROM Quiz_Questions_In_Set__c
                        WHERE Quiz_Question_Set__c = :questionSetId)
            ORDER BY CreatedDate DESC];
    }

    private static Set<String> compilePassedAnswerNames(List<ParsedQuestion> parsedQuestions) {
        Set<String> answerNames = new Set<String>();
        for(ParsedQuestion eachQuestion : parsedQuestions) {
            answerNames.add(eachQuestion.answerA);
            answerNames.add(eachQuestion.answerB);
            answerNames.add(eachQuestion.answerC);
            answerNames.add(eachQuestion.answerD);
            answerNames.add(eachQuestion.answerE);
        }
        return answerNames;
    }

    private static Map<String, Id> getExistingAnswers(Set<String> answerNames) {
        Map<Id, Quiz_Answer__c> answers = new Map<Id, Quiz_Answer__c>([SELECT Id, Name, Body__c
                                            FROM Quiz_Answer__c
                                            WHERE Name IN :answerNames]);
        Map<String, Id> result = new Map<String, Id>();
        for(Id eachAnswerId : answers.keySet()) {
            result.put(answers.get(eachAnswerId).Name, eachAnswerId);
        }
        return result;
    }

    private static Map<String, Id> createNewAnswers(List<ParsedQuestion> parsedQuestions, Map<String, Id> existingAnswerByName) {
        Map<String, Id> answerIdByName = new Map<String, Id>();
        List<Quiz_Answer__c> answersToCreate = new List<Quiz_Answer__c>();
        for(ParsedQuestion eachQuestion : parsedQuestions) {
            if(existingAnswerByName.keySet().contains(eachQuestion.answerA)) {
                answerIdByName.put(eachQuestion.answerA, existingAnswerByName.get(eachQuestion.answerA));
            } else if(eachQuestion.answerA != null) {
                answersToCreate.add(new Quiz_Answer__c(Body__c = eachQuestion.answerA, Name = eachQuestion.answerA));
            }

            if(existingAnswerByName.keySet().contains(eachQuestion.answerB)) {
                answerIdByName.put(eachQuestion.answerB, existingAnswerByName.get(eachQuestion.answerB));
            } else if(eachQuestion.answerB != null) {
                answersToCreate.add(new Quiz_Answer__c(Body__c = eachQuestion.answerB, Name = eachQuestion.answerB));
            }

            if(existingAnswerByName.keySet().contains(eachQuestion.answerC)) {
                answerIdByName.put(eachQuestion.answerC, existingAnswerByName.get(eachQuestion.answerC));
            } else if(eachQuestion.answerC != null) {
                answersToCreate.add(new Quiz_Answer__c(Body__c = eachQuestion.answerC, Name = eachQuestion.answerC));
            }

            if(existingAnswerByName.keySet().contains(eachQuestion.answerD)) {
                answerIdByName.put(eachQuestion.answerD, existingAnswerByName.get(eachQuestion.answerD));
            } else if(eachQuestion.answerD != null) {
                answersToCreate.add(new Quiz_Answer__c(Body__c = eachQuestion.answerD, Name = eachQuestion.answerD));
            }

            if(existingAnswerByName.keySet().contains(eachQuestion.answerE)) {
                answerIdByName.put(eachQuestion.answerE, existingAnswerByName.get(eachQuestion.answerE));
            } else if(eachQuestion.answerE != null) {
                answersToCreate.add(new Quiz_Answer__c(Body__c = eachQuestion.answerE, Name = eachQuestion.answerE));
            }
        }

        insert answersToCreate;
        for(Quiz_Answer__c eachAnswer : answersToCreate) {
            answerIdByName.put(eachAnswer.Name, eachAnswer.Id);
        }

        return answerIdByName;
    }

    private static Set<Id> createNewQuestions(List<ParsedQuestion> parsedQuestions, Map<String, Id> answerIdByName) {
        List<Quiz_Question__c> newQuestions = new List<Quiz_Question__c>();
        for(ParsedQuestion eachQuestion : parsedQuestions) {
            newQuestions.add(new Quiz_Question__c(
                Body__c = eachQuestion.body,
                Topic__c = eachQuestion.topic,
                Answer_Option_A__c = answerIdByName.get(eachQuestion.answerA),
                Answer_Option_B__c = answerIdByName.get(eachQuestion.answerB),
                Answer_Option_C__c = answerIdByName.get(eachQuestion.answerC),
                Answer_Option_D__c = answerIdByName.get(eachQuestion.answerD),
                Answer_Option_E__c = answerIdByName.get(eachQuestion.answerE),
                Correct_Answer__c = answerIdByName.get(eachQuestion.correctAnswer)
            ));
        }

        insert newQuestions;
        Set<Id> result = new Set<Id>();
        for(Quiz_Question__c eachQuestion : newQuestions) {
            result.add(eachQuestion.Id);
            // System.debug(eachQuestion);
        }
        return result;
    }

    private static void createNewQuestionsInSet(Set<Id> insertedQuestionIds, Id questionSetId) {
        List<Quiz_Questions_In_Set__c> questionsInSet = new List<Quiz_Questions_In_Set__c>();
        for(Id eachQuestionId : insertedQuestionIds) {
            questionsInSet.add(new Quiz_Questions_In_Set__c(Quiz_Question__c = eachQuestionId, Quiz_Question_Set__c = questionSetId));
        }
        insert questionsInSet;
    }

    public class ParseRequest {
        @AuraEnabled
        public Id documentId {get; set;}
        @AuraEnabled
        public String quizSetName {get; set;}
        @AuraEnabled
        public Integer timeLimit {get; set;}
    }

    public class ParsedQuestion {
        public String body;
        public String topic;
        public String answerA;
        public String answerB;
        public String answerC;
        public String answerD;
        public String answerE;
        public String correctAnswer;
    }
}
