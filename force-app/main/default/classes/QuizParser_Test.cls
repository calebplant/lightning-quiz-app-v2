@isTest
public with sharing class QuizParser_Test {

    private static String CSV_BODY = 'Body,Topic,A,B,C,D,E,Correct\n1 + 1 = ?,Addition,4,1,6,2,61,2\n5 + 5 = ?,Addition,10,29,50,12,25,10\n1 - 1 = ?,Subtraction,51,15,24,0,1,0\n10 - 5 = ?,Subtraction,8,5,2,6,2,5\n1 * 1 = ?,Multiplication,61,23,1,23,61,1\n5 * 5 = ?,Multiplication,100,20,10,200,25,25\n1 / 1 = ?,Division,0,1,-1,-10,10,1\n100 / 10 = ?,Division,1,10,100,1000,-100,10';

    @TestSetup
    static void makeData(){
        ContentVersion ContVerFile = new ContentVersion();
            ContVerFile.VersionData = Blob.valueOf(CSV_BODY);
            ContVerFile.Title = 'csvDoc'; 
            ContVerFile.ContentLocation= 's';
            ContVerFile.PathOnClient='csvDoc.csv';
        insert ContVerFile;
    }

    @isTest
    static void QuizParser_Test() {
        ContentDocument csvDoc = [SELECT Id FROM ContentDocument LIMIT 1];
        QuizParser.ParseRequest req = new QuizParser.ParseRequest();
        req.documentId = csvDoc.Id; 
        System.debug(req);

        Test.startTest();
        QuizPArser.importQuiz(req);
        Test.stopTest();

        // Debug
        System.debug('ANSWERS');
        List<Quiz_Answer__c> answers = [SELECT Id, NAme FROM Quiz_Answer__c];
        for(Quiz_Answer__c eachAnswer : answers) {
            System.debug(eachAnswer);
        }

        System.debug('QUESTIONS');
        List<Quiz_Question__c> questions = [SELECT Id, Body__c, Correct_Answer__r.Name FROM Quiz_Question__c];
        for(Quiz_Question__c eachQuestion : questions) {
            System.debug(eachQuestion.Body__c + ' => ' + eachQuestion.Correct_Answer__r.Name);
        }

        System.debug('QUESTIONS IN SET');
        List<Quiz_Questions_In_Set__c> questionsInSet = [SELECT Id, Quiz_Question__r.Body__c, Quiz_Question_Set__r.Name FROM Quiz_Questions_In_Set__c];
        for(Quiz_Questions_In_Set__c eachQuestion : questionsInSet) {
            System.debug(eachQuestion.Quiz_Question__r.Body__c + ' => ' + eachQuestion.Quiz_Question_Set__r.Name);
        }
    }
}
