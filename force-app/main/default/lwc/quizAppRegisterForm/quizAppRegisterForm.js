import { api, LightningElement } from 'lwc';

export default class QuizAppRegisterForm extends LightningElement {
    @api options;
    @api inputEmail;

    selectedValue;
    showRegisterModal;
    userInput;

    get questionSetOptions() {
        return this.options.map(eachSet => {
            return {label: eachSet.Name, value: eachSet.Id};
        });
    }

    get startBtnLabel() {
        return this.inputEmail ? 'Start' : 'Register';
    }


    // Handlers
    handleQuestionSetChange(event) {
        console.log('QuizAppRegisterForm :: handleQuestionSetChange');
        this.selectedValue = event.detail.value;
        console.log(this.selectedValue);
        this.dispatchEvent(new CustomEvent('setchange', {detail: event.detail.value}));
    } 

    handleSubmitRegister(event) {
        console.log('QuizAppRegisterForm :: handleSubmitRegister');
        let isValidEmail = this.inputEmail ? true : this.template.querySelector('lightning-input').checkValidity();
        if(isValidEmail) {
            this.dispatchEvent(new CustomEvent('submitemail', {detail: this.userInput}));
            this.showRegisterModal = false;
        }
    }

    handleRegisterClick(event) {
        console.log('QuizAppRegisterForm :: handleRegisterClick');
        if(this.inputEmail) {
            this.handleSubmitRegister();
        } else {
            this.showRegisterModal = true;
        }
    }

    handleCancelRegister(event) {
        console.log('QuizAppRegisterForm :: handleCancelRegister');
        this.showRegisterModal = false;
    }

    handleEmailChange(event) {
        console.log('QuizAppRegisterForm :: handleEmailChange');
        this.userInput = event.target.value;
    }
}