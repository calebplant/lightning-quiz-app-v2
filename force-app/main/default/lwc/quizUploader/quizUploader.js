import { LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent'
import importQuiz from '@salesforce/apex/QuizParser.importQuiz';

export default class QuizUploader extends LightningElement {

    quizName;
    timeLimit;

    get acceptedFormats() {
        return ['.csv'];
    }

    get uploadDisabled() {
        return this.quizName ? false : true;
    }

    // Handlers
    handleChange(event) {
        let name = event.currentTarget.name;
        switch(name) {
            case 'quizNameInput':
                // console.log(JSON.parse(JSON.stringify(event.detail.value)));
                this.quizName = event.detail.value;
                break;
            case 'timeLimitInput':
                // console.log(JSON.parse(JSON.stringify(event.detail)));
                this.timeLimit = event.detail.value;
                break;
        }
    }

    handleUploadFinished(event) {
        console.log('QuizUploader :: handleUploadFinished');
        console.log(JSON.parse(JSON.stringify(event.detail.files)));

        const uploadedFiles = event.detail.files;
        let payload = {
            documentId: uploadedFiles[0].documentId,
            quizSetName: this.quizName,
            timeLimit: this.timeLimit
        };
        console.log('payload:');
        console.log(payload);
        importQuiz({ req: payload})
        .then(result => {
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Success',
                    message: 'Imported quiz',
                    variant: 'success',
                }),
            );
        })
        .catch(error => {
            this.error = error;
            this.dispatchEvent(
                new ShowToastEvent({
                    title: 'Error',
                    message: JSON.stringify(error),
                    variant: 'error',
                }),
            );     
        })

    }
}