import { api, LightningElement } from 'lwc';

export default class QuizAppSummary extends LightningElement {

    @api checkedAnswers;
    @api timeElapsed;
    @api questions;

    topicScores;

    get amountCorrectLabel() {
        let total = 0;
        let correct = 0;
        Object.keys(this.checkedAnswers).forEach(key => {
            if(this.checkedAnswers[key] === true) {
                correct++;
            }
            total++;
        })
        return `${correct} / ${total}`;
    }

    get scoreLabel() {
        let total = 0;
        let correct = 0;
        Object.keys(this.checkedAnswers).forEach(key => {
            if(this.checkedAnswers[key] === true) {
                correct++;
            }
            total++;
        });
        return `${(correct / total * 100).toFixed(2)}%`;
    }

    get topicScore() {
        let result = [];
        let questionsByTopic = {};
        this.questions.forEach(eachQ => {
            questionsByTopic[eachQ.topic] = questionsByTopic[eachQ.topic] || [];
            questionsByTopic[eachQ.topic].push(eachQ.id);
        });
        console.log('questionsByTopic');
        console.log(questionsByTopic);

        // let tempResult = [];
        for (const [ eachTopic, questions ] of Object.entries(questionsByTopic)) {
            let totalCorrect = 0;
            let totalQuestions = 0;
            questions.forEach(eachQuestion => {
                if(this.checkedAnswers[eachQuestion] === true) {
                    totalCorrect++;
                }
                totalQuestions++;
            });
            result.push({
                label: eachTopic,
                numberCorrect: totalCorrect,
                numberTotal: totalQuestions
            });
        };
        // console.log('tempREsult:');
        // console.log(tempResult);
        return result;
    }

    // Handlers
    handleTryAgain(event) {
        console.log('quizAppSummary :: handleTryAgain');
        this.dispatchEvent(new CustomEvent('reset'));
    }

    handleTryOther(event) {
        console.log('quizAppSummary :: handleTryOther');
        this.dispatchEvent(new CustomEvent('tryother'));
    }
}